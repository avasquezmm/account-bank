package com.anvarco;


import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.Locale;

public class Accounts {

    private static ArrayList<Custumer> custumers = new ArrayList<Custumer>();
    private static int x = 0;

    public static Custumer getUser(int id){
        return Accounts.custumers.get(id);
    }

    public static void createCustumers(int x){
        Faker faker = new Faker(new Locale("es-MX"));

        for(int i = 1 ; i <= x; i++){
            Accounts.x = x;
            Accounts.custumers.add(new Custumer(i, faker.name().fullName(), faker.pokemon().name() , 1245, 10000075+i, faker.number().randomDouble(2,0,2500000)));
        }
    }

    public static String getName(int id){

        return Accounts.custumers.get(id).getName();
    }

    public static int getBankAccountNumber(int id){
        return Accounts.custumers.get(id).getBankAccountNumber();
    }

    public static String getUsername(int id){
        return Accounts.custumers.get(id).getUsername();
    }

    public static int getPassword(int id){
        return Accounts.custumers.get(id).getPassword();
    }

    public static double getBalance(int id){
        return Accounts.custumers.get(id).getBalance();
    }

    public static void print(){
        for (int i = 0; i< Accounts.x; i++) {
            System.out.print(i + "  ");
            System.out.print(Accounts.custumers.get(i).getName() +  "  ");
            System.out.print(Accounts.custumers.get(i).getBalance());
            System.out.println();

        }
    }


}
