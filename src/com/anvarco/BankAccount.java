package com.anvarco;


import javax.swing.JOptionPane;
import java.io.IOException;

public class BankAccount {

    double accountBalance;

    public void transation(String x){

        switch (x){
            case "withdraw":
                this.withdraw();
            break;
            case "deposit":
                this.deposit();
            break;
            case "transfer":
                this.transation();
            break;
            default:
                Helper.menu();
            break;
        }
    }

    public void setAccountBalance(double balance) {
        this.accountBalance = balance;
    }

    public void withdraw(){

        boolean ok;
        do {
            try {
                double withdraw = Double.parseDouble(JOptionPane.showInputDialog(null, "Ingrese el valor a retirar"));
                this.accountBalance = this.accountBalance - withdraw;
                ok = true;

            }catch (Exception e){
                System.err.println("Error al retirar!" + e.getMessage());
                JOptionPane.showMessageDialog(null, "Favor ingresar un valor valido", "Error de retiro", JOptionPane.ERROR_MESSAGE);
                ok = false;
            }
        }while (!ok);
    }

    public void deposit(){

        boolean ok;
        do {
            try {
                double deposit = Double.parseDouble(JOptionPane.showInputDialog(null, "Ingrese el valor a depositar"));
                this.accountBalance = this.accountBalance + deposit;
                ok = true;

            }catch (Exception e){
                System.err.println("Error al depositar!" + e.getMessage());
                JOptionPane.showMessageDialog(null, "Favor ingresar un valor valido", "Error de deposito", JOptionPane.ERROR_MESSAGE);
                ok = false;
            }
        }while (!ok);
    }

    public void transation(){

        boolean ok;
        do {
            try {
                double transfer = Double.parseDouble(JOptionPane.showInputDialog(null, "Ingrese el valor a tranferir"));
                this.accountBalance = this.accountBalance - transfer;
                ok = true;

            }catch (Exception e){
                System.err.println("Error al transferir" + e.getMessage());
                JOptionPane.showMessageDialog(null, "Favor ingrese un valor valido", "Error de transferencia", JOptionPane.ERROR_MESSAGE);
                ok = false;
            }
        }while (!ok);

    }
}
