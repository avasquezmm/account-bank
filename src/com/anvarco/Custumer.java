package com.anvarco;


public class Custumer {

    private int id;
    private String name;
    private String username;
    private int password;
    private int bankAccountNumber;
    private double balance;

    public Custumer(){

    }

    public Custumer(int id, String name, String username, int password, int bankAccountNumber, double balance) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.bankAccountNumber = bankAccountNumber;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public int getPassword() {
        return password;
    }

    public int getBankAccountNumber() {
        return bankAccountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public boolean loginAccount(int id, int pw){

        if (pw == Accounts.getPassword(id)){
            return true;
        }

        return false;
    }



}
